package com.mustakimov.test.cftcalc;

import android.util.Log;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Stack;

import static com.mustakimov.test.cftcalc.CalcPresenter.isNumber;

/*
*    Класс, в котором производятся вычисления выражений.
*    Сначала выражение преобразуется в обратную польскую запись,
*    а затем по правилам польской записи вычисляется результат.
*    Класс содержит два поля типа стэк и одно поле типа очередь, необходимые для
*    преобразований в польскую запись и вычислений.
*/

public class CalcCore {

    Stack<String>  symbols, stack; //стэки для хранения символов и промежуточных вычислений
    Queue<String> numbers; //очередь для хранения обратной польской записи



    /*Функция, вызываемая в презентере
    *  На входе - массив элементов типа строка, составляющий запись выражения
    *  На выходе - результат типа строка
    *  В данной функции инициализируются поля класса.
    *
    */
    public String Canculate(String str[]){

        numbers = new ArrayDeque<>();
        symbols = new Stack<>();
        stack = new Stack<>();

        /*
        * Перебор элементов массива.
        * Каждый элемент проверяется по условию.
        * Если элемент не является одним из знаков выражения (+,-,*,/,^, правая и левая скобки), то
        * автоматически добавляется в очередь numbers.
        * Иначе в соответствии со знаком происходит новая проверка условий.
        *
        * */

        cirle: for(String x:str){
            switch (x){
                default: {numbers.add(x); continue cirle;} //Если элемент - число
                case "(": //Если элемент - левая скобка, то он добавляется в стэк символов
                    symbols.push(x); continue;

                case "+": //Если элемент - "+"
                    {
                        if(symbols.empty()) {symbols.push(x);continue cirle;} //Если стэк символов пуст - добавить + в стэк и продолжить перебор
                        else
                            switch (symbols.peek()){ //иначе - проверка последнего элемента стэка символов
                                case "(": {
                                    symbols.pop(); //при обнаружении открывающейся скобки скобка выбрасывается из стэека символов
                                    symbols.push(x); //далее в стэк символов добавляется "+"
                                    continue cirle;} //перебор элементов продолжается
                                case "+": {
                                    numbers.add(symbols.pop()); //Если предыдущий знак тоже "+", то он выгружается в польскую запись
                                    symbols.push(x); //далее в стэк символов добавляется "+"
                                    continue cirle;}  //перебор элементов продолжается
                                case "-": {
                                    numbers.add(symbols.pop());  //Если предыдущий знак "-", то он выгружается в польскую запись
                                    symbols.push(x); //далее в стэк символов добавляется "+"
                                    continue cirle;} //перебор элементов продолжается
                                case "*": {
                                    numbers.add(symbols.pop()); //Если предыдущий знак "умножение", то он выгружается в польскую запись
                                    symbols.push(x); //далее в стэк символов добавляется "+"
                                    continue cirle;} //перебор элементов продолжается
                                case "/": {
                                    numbers.add(symbols.pop()); //Если предыдущий знак "деление", то он выгружается в польскую запись
                                    symbols.push(x); //далее в стэк символов добавляется "+"
                                    continue cirle;} //перебор элементов продолжается
                                case "^": {
                                    numbers.offer(symbols.pop()); //Если предыдущий знак "степень", то он выгружается в польскую запись
                                    symbols.push(x); //далее в стэк символов добавляется "+"
                                    continue cirle;} //перебор элементов продолжается
                                default: symbols.push(x);continue cirle;

                            }

                    }

                case "-": //при обнаружении знака "минус" алгоритм аналогичен алгоритму при обнаружении знака "+"
                    {
                        if(symbols.empty()) {symbols.push(x); continue;}
                        else
                        switch (symbols.peek()){
                            case "(": {
                                symbols.pop();
                                symbols.push(x);
                                continue cirle;}
                            case "+": {
                                numbers.offer(symbols.pop());
                                symbols.push(x);
                                continue cirle;}
                            case "-": {
                                numbers.offer(symbols.pop());
                                symbols.push(x);
                                continue cirle;}
                            case "*": {
                                numbers.offer(symbols.pop());
                                symbols.push(x);
                                continue cirle;}
                            case "/": {
                                numbers.offer(symbols.pop());
                                symbols.push(x);
                                continue cirle;}
                            case "^": {
                                numbers.offer(symbols.pop());
                                symbols.push(x);
                                continue cirle;}
                            default: symbols.push(x);continue cirle;

                        }
                    }
                case "*": //при обнаружении знака "умножить"
                    {
                        if(symbols.empty()) {symbols.push(x); continue cirle;} //Если стэк символов пуст - добавить * в стэк и продолжить перебор
                        else
                        switch (symbols.peek()){ //иначе - проверка последнего элемента стэка символов
                            case "(": {
                                symbols.pop(); //при обнаружении открывающейся скобки скобка выбрасывается из стэека символов
                                symbols.push(x); //далее в стэк символов добавляется "*"
                                continue cirle;}
                            case "+": symbols.push(x);continue cirle; //При обнаружении знака "+" знак "*" добавляется в стэк символов
                                //так как операция умножения более приоритетная
                            case "-": symbols.push(x);continue cirle; //Аналогично со знаком "-"
                            case "*": {
                                numbers.offer(symbols.pop()); //Если предыдущий знак "умножение", то он выгружается в польскую запись
                                symbols.push(x); //далее в стэк символов добавляется "*"
                                continue cirle;}
                            case "/": {
                                numbers.offer(symbols.pop()); //Если предыдущий знак "деление", то он выгружается в польскую запись
                                symbols.push(x); //далее в стэк символов добавляется "*"
                                continue cirle;}
                            case "^": {
                                numbers.offer(symbols.pop()); //Если предыдущий знак "степень", то он выгружается в польскую запись
                                symbols.push(x); //далее в стэк символов добавляется "*"
                                continue cirle;}
                            default: symbols.push(x);continue cirle;

                        }
                    }
                case "/": //при обнаружении знака деления запускается алгоритм, аналогичный знаку умножения
                    {
                        if(symbols.empty()) {symbols.push(x); continue cirle;}
                        else
                        switch (symbols.peek()){
                            case "(": {
                                symbols.pop();
                                symbols.push(x);
                                continue cirle;}
                            case "+": symbols.push(x); continue cirle;
                            case "-": symbols.push(x); continue cirle;
                            case "*": {
                                numbers.offer(symbols.pop());
                                symbols.push(x);
                                continue cirle;}
                            case "/": {
                                numbers.offer(symbols.pop());
                                symbols.push(x);
                                continue cirle;}
                            case "^": {
                                numbers.offer(symbols.pop());
                                symbols.push(x);
                                continue cirle;}
                            default: symbols.push(x); continue cirle;

                        }
                    }
                case "^": //знак степени при обнаружении любого знака кроме открывающей скобки
                    //загружается в стэк символов, так как имеет больший приоритет
                {
                    if(symbols.empty()) {symbols.push(x); continue cirle;}
                    else
                        switch (symbols.peek()){
                            case "(": {
                                symbols.pop();
                                symbols.push(x);
                                continue cirle;}
                            case "+": {

                                symbols.push(x);
                                continue cirle;}
                            case "-": {

                                symbols.push(x);
                                continue cirle;}
                            case "*": {

                                symbols.push(x);
                                continue cirle;}
                            case "/": {

                                symbols.push(x);
                                continue cirle;}
                            default: symbols.push(x);continue cirle;

                        }
                }
                case ")": //при обнаружении закрывающей скобки из стэка символов последний символ загружается в польскую запись
                {
                    if(symbols.empty()) continue cirle;
                    else
                    switch (symbols.peek()){
                        case "(": continue cirle; //Если последний символ - открывающая скобка, то в польскую запись ничего не загружается
                        case "+": numbers.offer(symbols.pop()); continue cirle;
                        case "-": numbers.offer(symbols.pop()); continue cirle;
                        case "*": numbers.offer(symbols.pop()); continue cirle;
                        case "/": numbers.offer(symbols.pop()); continue cirle;
                        case "^": numbers.offer(symbols.pop()); continue cirle;
                        default: continue cirle;

                    }
                }
            }
        }
        /*
        * Все оставшиеся в стэке символов знаки выгружаются в польскую запись
        * */

        while(!symbols.empty()) numbers.add(symbols.pop());

        return calculate(); //запускается процедура вычисления выражения
    }
    
    private String calculate(){

        //переменные для хранения результата операции и буфера соответственно
        Double res = 0.0;
        Double buf = 0.0;

        //Перебор по очереди польской записи
        while(!numbers.isEmpty()){
            if(isNumber(numbers.element())) //Если первый элемент очереди - число, то загружаем его в стэк
                stack.push(numbers.poll());
            else { //иначе проводим вычисление по условию
                switch (numbers.poll()){

                    /*
                    * При обнаружении знака степени
                    * Из очереди польской записи выгружаются два числа
                    * Затем последнее выгруженное возводится в степень, равную первому выгруженному
                    * Результат вычисления записывается в стэк
                    * */
                    case "^": {

                        buf = Double.parseDouble(stack.pop()); //использование буфера для хранения первого числа
                        res = Math.pow(Double.parseDouble(stack.pop()),buf);
                        stack.add(stack.size(), ((res).toString()));
                        Log.d("В степень: ",res.toString());
                        continue;
                    }
                    /*
                     * При обнаружении знака умножения
                     * Из очереди польской записи выгружаются два числа
                     * Затем они перемножаются
                     * * Результат вычисления записывается в стэк
                     * */
                    case "*": {

                        res = Double.parseDouble(stack.pop()) * Double.parseDouble(stack.pop());
                        stack.add(stack.size(), ((res).toString()));
                        Log.d("Умножение: ",res.toString());
                        continue;
                    }

                    /*
                     * При обнаружении знака деления
                     * Из очереди польской записи выгружаются два числа
                     * Затем последнее выгруженное делится (делимое), на первое выгруженное (делитель)
                     * Частное записывается в стэк
                     * */
                    case "/": {
                        buf = Double.parseDouble(stack.pop());
                        res =  Double.parseDouble(stack.pop())/buf;
                        stack.add(stack.size(), ((res).toString()));
                        Log.d("Деление: ",res.toString());
                        continue;
                    }
                    /*
                     * При обнаружении знака сложения
                     * Из очереди польской записи выгружаются два числа
                     * Затем оба числа складываются
                     * * Результат вычисления записывается в стэк
                     * */

                    case "+": {
                        buf = Double.parseDouble(stack.pop());
                        res = Double.parseDouble(stack.pop()) + buf;
                        stack.add(stack.size(), ((res).toString()));
                        Log.d("Сложение: ",res.toString());
                        continue;
                    }

                    /*
                     * При обнаружении знака вычитания
                     * Из очереди польской записи выгружаются два числа
                     * Затем последнее выгруженное уменьшается на число, равное первому выгруженному
                     * * Результат вычисления записывается в стэк
                     * */
                    case "-": {
                        buf = Double.parseDouble(stack.pop());
                        res = Double.parseDouble(stack.pop()) - buf;
                        stack.add(stack.size(), ((res).toString()));
                        Log.d("Вычитание: ",res.toString());
                        continue;
                    }
                }
            }
        }
        /*
        * В результате в стэке остается результат вычисления всего выражения
        * */
        return stack.pop();
    }
}

package com.mustakimov.test.cftcalc;

import android.util.Log;

import java.util.ArrayList;

// класс для связи класса-вычислителя и класса-активити
public class CalcPresenter {

    static CalcCore core = new CalcCore(); //связь с классом-вычислителем

    private ArrayList<String> IncStr = new ArrayList<String>() {    }; //массив элементов выражения

    //функция-обработчик нажатий
    // при нажатии выполняется проверка id нажатой кнопки и в соответствии с ним в массив эелементов добавляется символ
    public void ConvertEnteredSymbol(int sym){
        switch(sym){
            case R.id.oneButton: addSymbol("1"); break; //для добавления цифр и дробной точки используется функция addSymbol
            case R.id.twoButton: addSymbol("2");break;
            case R.id.threeButton: addSymbol("3");break;
            case R.id.fourButton: addSymbol("4");break;
            case R.id.fiveButton: addSymbol("5");break;
            case R.id.sixButton: addSymbol("6");break;
            case R.id.sevenButton: addSymbol("7");break;
            case R.id.eightButton: addSymbol("8");break;
            case R.id.nineButton: addSymbol("9");break;
            case R.id.zeroButton: addSymbol("0");break;
            case R.id.multButton: IncStr.add("*");break; //знаки выражений +,-,*,/ сразу же добавляются в массив
            case R.id.plusButton: IncStr.add("+");break;
            case R.id.minusButton: IncStr.add("-");break;
            case R.id.disButton: IncStr.add("/");break;
            case R.id.leftScopeButton: IncStr.add("(");break;
            case R.id.rightScopeButton: IncStr.add(")");break;
            case R.id.powButton: IncStr.add("^");break;
            case R.id.clearButton: IncStr.clear();break;
            case R.id.dotButton: addSymbol("."); break;
            //при нажатии кнопки "=" поле выражения очищается и в него записывается результат вычисления
            case R.id.resultButton:{
                    String buf[] = {};
                    buf = IncStr.toArray(buf);
                    IncStr.clear();
                    //for(String i:buf) IncStr.add(i);
                    IncStr.add(core.Canculate(buf));
            }   break;

            default: IncStr.add(null); break;

        }
    }

    public String getStr(){
        String str = "";

        for(String i : IncStr){
            str += i;
        }

        return str;
    }

    //Функция добавления символа
    /*
      * Если ранее были введены значения и массив элементов НЕ пуст, то
       * выполняется проверка последнего символа:
       *     Если последний элемент - число, цифра или дробная точка, то к этому элементу прибавляется цифра,
       *     соответствующая нажатой кнопке.
       *     Иначе (если последний элемент - знак вычисления) к массиву элементов прибавляется новый элемент
      * Иначе (если ранее не были введены значения) в массив элементов добавляется новый элемент
      */

    private void addSymbol(String symbol){
       if(IncStr.size()>0) {
           if (isNumber(IncStr.get(IncStr.size() - 1))) {

               IncStr.add(IncStr.size()-1,IncStr.remove(IncStr.size()-1)+symbol);
               Log.d("Длинное число",IncStr.get(IncStr.size()-1));

           } else IncStr.add(symbol);
       } else IncStr.add(symbol);


    }

    //Проверка символа на то, является ли он цифрой или частью числа

    public static boolean isNumber(String sym){
        return (sym!="-" && sym!="+" && sym!="*" && sym!="/" && sym!="(" && sym!=")" && sym!="^")?true:false;
    }

}

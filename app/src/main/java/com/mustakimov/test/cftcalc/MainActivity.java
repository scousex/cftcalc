package com.mustakimov.test.cftcalc;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    CalcPresenter calcPresenter; //класс для обработки нажатий и передачи команд в класс-вычислитель
    TextView textField; //текстовое поле 'numbers' для обновления значений поля text

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textField = (TextView) findViewById(R.id.numbers); //регистрация текстового поля
        calcPresenter = new CalcPresenter(); //привязка презентера
    }
    // обработчик события нажатия на кнопку (одну из кнопок на клавитуре)
      public void onBtnsClick(android.view.View view){

        calcPresenter.ConvertEnteredSymbol(view.getId()); //запуск обработки нажатий в презентере

        textField.setText(calcPresenter.getStr()); //обновление отображаемых значений

    }

}

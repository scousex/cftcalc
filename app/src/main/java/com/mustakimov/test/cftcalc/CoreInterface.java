package com.mustakimov.test.cftcalc;

public interface CoreInterface {
    String parseString();
    double calculateResult();
    String showResult();
}
